from env import Env
from agents import AgentMinMax
from human import Human

import sys
sys.setrecursionlimit(1500)

def main(p1, p2, env, draw = True):
    current_player = None

    while not env.ended:
        if current_player == p1:
            current_player = p2
        else:
            current_player = p1

        current_player.move(env)
        env.game_over()

        if draw:
            env.draw_board()
        
        if env.ended:
            print(f"The winner is: {env.winner}")
            break

def play():
    env = Env()
    # env.simplify_board()
    p1 = AgentMinMax()
    p2 = Human()

    p1.set_player(env.yellow)
    p2.set_player(env.red)
    env.draw_board()
    main(p1, p2, env)

if __name__ == "__main__":
    play()
