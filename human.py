import numpy as np

class Human:
    def __init__(self):
        pass

    def set_player(self, player):
        self.player = player

    def move(self, env):
        while True:
            move = input("Enter column to play [0-6]:")
            move = int(move)
            if env.is_valid_move(move):
                env.make_move(move, self.player)
                break
