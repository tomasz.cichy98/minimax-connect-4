import numpy as np

WIDTH = 7
HEIGHT = 6

class Env:
    def __init__(self):
        self.board = np.zeros((HEIGHT, WIDTH))
        self.red = -1                        # player, yellow goes first
        self.yellow = 1
        self.winner = None
        self.ended = False
        self.num_states = 3**(WIDTH*HEIGHT)

    def simplify_board(self):
        self.make_move(0,1)
        self.make_move(1,1)
        self.make_move(3,1)
        self.make_move(2,-1)
        self.make_move(5,1)
        self.make_move(6,-1)
        self.make_move(4,-1)
        self.make_move(1,-1)
        self.make_move(0,-1)
        self.make_move(2,-1)
        self.make_move(3,1)
        self.make_move(1,-1)
        self.make_move(0,1)
        self.make_move(1,1)
        self.make_move(3,1)
        self.make_move(2,1)
        self.make_move(5,1)
        self.make_move(6,-1)
        self.make_move(4,1)
        self.make_move(1,-1)
        self.make_move(0,1)
        self.make_move(2,-1)
        self.make_move(3,-1)
        self.make_move(1,-1)

    def is_empty(self, i, j):
        return self.board[i, j] == 0
    
    def is_pos_final(self, position):
        pass
    
    def reward(self, player):                # for future AI implementations
        if not self.game_over():             # give reward only after the game is over
            return 0
        return 1 if self.winner == player else -1

    def is_valid_move(self, col):
        board_col = self.board[:, col]
        if sum(abs(board_col)) == HEIGHT:
            return False
        if col > WIDTH - 1:
            return False
        return True
    
    def possible_moves(self):
        moves = []
        coords = []
        for i in range(WIDTH):
            if self.is_valid_move(i):
                moves.append(i)
                
        for col in moves:
            board_col = self.board[:, col]

            for i in reversed(range(len(board_col))):
                if board_col[i] == 0:
                    coords.append((i, col))
                    break

        return coords

    def make_move(self, col, player, verbose = False):
        board_col = self.board[:, col]
        if not self.is_valid_move(col):
            return

        for i in reversed(range(len(board_col))):
            if board_col[i] == 0:
                self.board[i, col] = player
                # self.game_over()
                if verbose:
                    print(f"Player: {player},\t move (column): {col}")
                return self.board

    def game_over(self):        
        # horizontal
        for row in range(HEIGHT):           # row
            for col in range(3, WIDTH):     # column (cell)
                if self.board[row, col - 3] + self.board[row, col - 2] + self.board[row, col - 1] + self.board[row, col] in [-4, 4]:
                    self.winner = self.board[row, col]
                    self.ended = True
                    return True

        # vertical
        for col in range(WIDTH):
            for row in range(3, HEIGHT):
                if self.board[row - 3, col] + self.board[row - 2, col] + self.board[row - 1, col] + self.board[row, col] in [-4, 4]:
                    self.winner = self.board[row, col]
                    self.ended = True
                    return True

        # diagonal
        # decreasing
        for row in range(3, HEIGHT):        # row
            for col in range(3, WIDTH):     # column (cell)
                if self.board[row - 3, col - 3] + self.board[row - 2, col - 2] + self.board[row - 1, col - 1] + self.board[row, col] in [-4, 4]:
                    self.winner = self.board[row, col]
                    self.ended = True
                    return True

        # increasing
        for row in range(3, HEIGHT):        # row
            for col in range(WIDTH-3):      # column (cell)
                if self.board[row - 3, col + 3] + self.board[row - 2, col + 2] + self.board[row - 1, col + 1] + self.board[row, col] in [-4, 4]:
                    self.winner = self.board[row, col]
                    self.ended = True
                    return True
        
        # check draw
        if np.all((self.board == 0) == False):
            self.winner = None
            self.ended = True
            return True

    def set_board(self, position):
        self.board = position
        self.game_over()

    def draw_board(self):
        for row in range(HEIGHT):
            print("------------------------------")
            print(f"{row}|", end = "")
            for col in range(WIDTH):
                if self.board[row, col] == self.red:
                    print(" R |", end = "")
                elif self.board[row, col] == self.yellow:
                    print(" Y |", end = "")
                else:
                    print("   |", end = "")
            print("")
        print("------------------------------")
        print(" |", end="")
        for col in range(WIDTH):
            print(f" {col} |", end = "")
        print("")
