import numpy as np
import time

class AgentMinMax:
    def __init__(self):
        self.is_max = True
        self.player = None


    def minimax(self, env, depth, alpha, beta, is_max_player = True):
        if env.game_over():
            # deal with Null winner (draw)
            value = env.winner
            if env.winner == None:
                value = 0
            # reset
            env.winner = None
            env.ended = False
            return value

        possible_moves = env.possible_moves()

        if is_max_player:
            max_eval = float("-inf")
            for move in possible_moves:
                env.board[move[0], move[1]] = 1
                curr_eval = self.minimax(env = env, depth = depth - 1, alpha = alpha, beta = beta, is_max_player = False)
                env.board[move[0], move[1]] = 0
                max_eval = max(max_eval, curr_eval)
                # alpha beta pruning
                alpha = max(alpha, curr_eval)
                if beta >= alpha:
                    break
            return max_eval
        else:
            min_eval = float("inf")
            for move in possible_moves:
                env.board[move[0], move[1]] = -1
                curr_eval = self.minimax(env = env, depth = depth - 1, alpha = alpha, beta = beta, is_max_player =  True)
                env.board[move[0], move[1]] = 0
                min_eval = min(min_eval, curr_eval)
                # alpha beta pruning
                beta = min(beta, curr_eval)
                if alpha >= beta:
                    break
            return min_eval


    def move(self, env):
        print("Minimaxing...")
        start = time.time()

        move_value = {}

        possible_moves = env.possible_moves()

        for move in possible_moves:
            env.board[move[0], move[1]] = 1                # make a move then minimax from it
            move_value[move] = self.minimax(env, 0, float("-inf"), float("inf"), is_max_player = False)
            env.board[move[0], move[1]] = 0                 # revert the move
       
        next_move = max(move_value, key = move_value.get)
        env.board[next_move[0], next_move[1]] = 1

    def set_player(self, player):
        self.player = player