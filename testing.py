import numpy as np

from env import Env


env = Env()
# env.board[0,5] = -1
# env.board[1,6] = -1
# env.board[3,0] = 1
# env.board[2,1] = 1
# env.board[1,2] = 1
# env.board[0,3] = 1

# env.draw_board()

# print(env.first_non_zero_row_in_col(2))
# env.pop_column(1)
# env.draw_board()
# env.make_move(1,-1)
# env.draw_board()
# print((env.game_over(), env.winner))

env.simplify_board()
env.draw_board()
print((env.game_over(), env.winner))
